<?php namespace App;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model{

 protected $table = 'Categories';
 protected $primaryKey = 'id';
 protected $fillable = ['id','CategoryName','Description'];
 protected $hidden = ['created_at','updated_at','Picture'];

}
