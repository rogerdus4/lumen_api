<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



  $router->get('/categories', 'CategoriesController@index');
  $router->get('/categories/{categories}', 'CategoriesController@getCategories');
  $router->post('/categories', 'CategoriesController@createCategories');
  $router->put('/categories/id/{categories}','CategoriesController@updateCategories');
  //$router->delete('/categories/categorie/{id}','CategoriesController@deleteCategories');
?>
